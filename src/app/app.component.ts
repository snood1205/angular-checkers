import {Component} from '@angular/core'

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  range = (new Array(8)).fill(0).map((_, i) => i)

  squareClass (column: number, row: number): string {
    if (row % 2 === (column % 2)) {
      return 'blue-square'
    } else {
      return 'grey-square'
    }
  }
}
