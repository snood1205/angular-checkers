import {Color, Occupant} from './occupant.model'

export class Square {
  constructor (public row: number,
               public column: number,
               public occupant?: Occupant) {
  }

  isEmpty (): boolean {
    return this.occupant == null
  }

  get text (): string {
    if (this.isEmpty()) {
      return ''
    } else if (this.occupant.color === Color.BLACK) {
      return '●'
    } else {
      return '○'
    }
  }
}
