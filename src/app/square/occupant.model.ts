export class Occupant {
  constructor (public color: Color) {}
}

export enum Color { WHITE, BLACK, UNOCCUPIED }
