import {Component, Input, OnInit} from '@angular/core'
import {Occupant} from './occupant.model'
import {Square} from './square.model'

@Component({
  selector: 'app-square',
  templateUrl: './square.component.html',
  styleUrls: ['./square.component.css']
})
export class SquareComponent implements OnInit {
  @Input() row: number
  @Input() column: number
  @Input() occupant?: Occupant
  square: Square

  constructor () { }

  ngOnInit () {
    this.square = new Square(this.row, this.column, this.occupant)
  }

}
