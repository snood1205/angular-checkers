import { AngularCheckersPage } from './app.po';

describe('angular-checkers App', () => {
  let page: AngularCheckersPage;

  beforeEach(() => {
    page = new AngularCheckersPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
